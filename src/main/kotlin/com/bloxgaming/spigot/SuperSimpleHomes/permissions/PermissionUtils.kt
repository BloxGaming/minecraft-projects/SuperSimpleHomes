package com.bloxgaming.spigot.SuperSimpleHomes.permissions

import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import org.bukkit.entity.Player
import org.jetbrains.kotlin.utils.addToStdlib.ifNotEmpty

/**
 * PermissionUtils
 * @author Gregory Maddra
 * 2017-01-28
 * @version 2021-03-15
 */
object PermissionUtils {

    fun hasHomePermission(player: Player, id: Int, checks: Int = SuperSimpleHomes.permCheckIterations): Boolean {
        (id..(id + checks))
                .filter { player.hasPermission("homes.teleport.$it") }
                .ifNotEmpty { return true }
        return false
    }
}