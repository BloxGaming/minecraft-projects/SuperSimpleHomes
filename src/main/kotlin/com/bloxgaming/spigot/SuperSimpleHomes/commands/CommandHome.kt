package com.bloxgaming.spigot.SuperSimpleHomes.commands

import com.bloxgaming.spigot.SuperSimpleHomes.Home
import com.bloxgaming.spigot.SuperSimpleHomes.SuperSimpleHomes
import com.bloxgaming.spigot.SuperSimpleHomes.db.Utils
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitRunnable

/**
 * CommandHome
 * @author Gregory Maddra
 * 2017-01-21
 * @version 2021-03-15
 */
class CommandHome : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if(sender is Player){
            when (args.size) {
                0 -> {
                    val home = Utils.getHome(sender)
                    if (home.valid && home.perms) {
                        sender.sendMessage("${ChatColor.GREEN}You will be teleported in ${ChatColor.WHITE}${SuperSimpleHomes.teleportDelay}" +
                                "${ChatColor.GREEN} seconds!")
                        object: BukkitRunnable() {
                            override fun run() {
                                sender.teleport(home.location)
                            }
                        }.runTaskLater(SuperSimpleHomes.plugin as Plugin, 20L * SuperSimpleHomes.teleportDelay)
                    } else if (!home.valid) {
                        sender.sendMessage("${ChatColor.RED}Home does not exist")
                    } else if (!home.perms) {
                        sender.sendMessage("${ChatColor.RED}Insufficient permissions")
                    }
                }
                1 -> {
                    var Home: Home
                    try {
                        Home = Utils.getHome(sender, args[0].toInt())
                    } catch (ex: NumberFormatException) {
                        Home = Utils.getHomeByName(sender, args[0])
                    }
                    if (Home.valid && Home.perms) {
                        sender.sendMessage("${ChatColor.GREEN}You will be teleported in ${ChatColor.WHITE}${SuperSimpleHomes.teleportDelay}" +
                                "${ChatColor.GREEN} seconds!")
                        object: BukkitRunnable() {
                            override fun run() {
                                sender.teleport(Home.location)
                            }
                        }.runTaskLater(SuperSimpleHomes.plugin as Plugin, 20L * SuperSimpleHomes.teleportDelay)
                    } else if (!Home.valid) {
                        sender.sendMessage("${ChatColor.RED}Home does not exist")
                    } else if (!Home.perms) {
                        sender.sendMessage("${ChatColor.RED}Insufficient permissions")
                    }
                }
                else -> return false
            }
        }
        else if (sender is ConsoleCommandSender) {
            sender.sendMessage("Only players can use this command!")
        }
        return true
    }
}