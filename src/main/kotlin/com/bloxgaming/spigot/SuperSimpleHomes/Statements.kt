package com.bloxgaming.spigot.SuperSimpleHomes

/**
 * Statements
 * @author Gregory Maddra
 * 2017-01-21
 */
object Statements {

    const val CREATE_HOMES_TABLE = "CREATE TABLE IF NOT EXISTS Homes(id int AUTO_INCREMENT PRIMARY KEY, owner varchar(36), homeNum int, name varchar(255), x double, y double, z double, world uuid)"
    const val FIND_HOME = "SELECT x,y,z,world FROM Homes WHERE owner=? AND homeNum=? LIMIT 1"
    const val ADD_HOME = "INSERT INTO Homes(owner, homeNum, name, x, y, z, world) VALUES(?, ?, ?, ?, ?, ?, ?)"
    const val UPDATE_HOME = "UPDATE Homes SET x = ?, y = ?, z = ?, world = ?, name=? WHERE owner=? AND homeNum=? LIMIT 1"
    const val DELETE_HOME = "DELETE FROM Homes WHERE owner=? AND homeNum=? LIMIT 1"
    const val DELETE_HOME_BY_NAME = "DELETE FROM Homes WHERE owner=? and name=? LIMIT 1"
    const val FIND_ALL_HOMES = "SELECT homeNum,name,x,y,z,world FROM Homes WHERE owner=? ORDER BY homeNum ASC"
    const val FIND_HOME_BY_NAME = "SELECT homeNum,x,y,z,world FROM Homes WHERE owner=? AND name=? LIMIT 1"
    const val GET_HOME_NAME = "SELECT name FROM Homes WHERE owner=? AND id=? LIMIT 1"

    //Updates Homes table to store names. 1.1 update
    const val UPDATE_HOMES_NAMES = "ALTER TABLE Homes ADD COLUMN IF NOT EXISTS name varchar(255) AFTER homeNum"
}