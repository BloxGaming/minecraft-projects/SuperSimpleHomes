package com.bloxgaming.spigot.SuperSimpleHomes

import com.bloxgaming.spigot.SuperSimpleHomes.commands.CommandDelHome
import com.bloxgaming.spigot.SuperSimpleHomes.commands.CommandHome
import com.bloxgaming.spigot.SuperSimpleHomes.commands.CommandHomes
import com.bloxgaming.spigot.SuperSimpleHomes.commands.CommandSetHome
import com.bloxgaming.spigot.SuperSimpleHomes.db.ConnectionPool
import com.bloxgaming.spigot.SuperSimpleHomes.db.Utils
import com.zaxxer.hikari.HikariDataSource
import org.bukkit.plugin.java.JavaPlugin
import java.sql.Connection

/**
 * Main
 * @author Gregory Maddra
 * 2017-01-21
 * @version 2021-03-15
 */
class SuperSimpleHomes : JavaPlugin(){

    companion object {
        var fileDir: String? = ""
        lateinit var pool: HikariDataSource
        var plugin: SuperSimpleHomes? = null
        var teleportDelay: Int = 3
        var permCheckIterations: Int = 5

    }

    override fun onEnable() {
        fileDir = dataFolder.path
        plugin = this

        ConnectionPool.init()
        pool = ConnectionPool.pool
        var connection: Connection? = null
        try {
            connection = pool.connection
            val statement = connection.prepareStatement(Statements.CREATE_HOMES_TABLE)
            statement.execute()
            statement.close()
        } finally {
            connection?.close()
        }

        this.getCommand("home")?.setExecutor(CommandHome())
        this.getCommand("sethome")?.setExecutor(CommandSetHome())
        this.getCommand("delhome")?.setExecutor(CommandDelHome())
        this.getCommand("homes")?.setExecutor(CommandHomes())

        val config = this.config
        config.addDefault("TeleportDelay", 3)
        config.addDefault("permCheckIterations", 5)
        config.addDefault("databaseVersion", "")
        config.options().copyDefaults(true)
        this.saveDefaultConfig()

        teleportDelay = config.getInt("TeleportDelay", 3)
        permCheckIterations = config.getInt("permCheckIterations", 5)
        handleDBUpdates(config.getString("databaseVersion", "")!!) //This should always be at least "" and not null
        config.set("databaseVersion", description.version)
        this.saveConfig()


        logger.info("Bloxgate's Super Simple Homes is Enabled")
    }

    override fun onDisable() {
        pool.close()
        logger.info("Disabled Bloxgate's Super Simple Homes")
    }

    private fun handleDBUpdates(version: String) {
        val oldVer = if (version == "") {
            listOf("1", "0", "0")
        } else {
            version.split(".")
        }

        if (oldVer[0] == "1") {
            if (oldVer[1] == "0") {
                Utils.runPreparedSQLStatement(Statements.UPDATE_HOMES_NAMES)
                logger.info("1.0.0 -> 1.1 database upgrade performed!")
            }
        }
    }

}